# Présentation de [Stylo](https://stylo.huma-num.fr)

Intervention « Stylo, un éditeur de texte pour les SHS » (INHA «&nbsp;Les lundis du numérique&nbsp;» &ndash; 12 avril 2021)

&rarr; [Voir la présentation](https://nsauret.gitpages.huma-num.fr/stylo-inha/)
