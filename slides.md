layout: true
name: layfplab

.logo[![logo](https://fplab.parisnanterre.fr/assets/img/logos/logoFPLab.svg)]

---

title: Gitlab pour les SHS
description: Nicolas Sauret, 24 mars 2022, Ateliers FP Lab
theme: style.css
class: left, middle, firstslide
template: layfplab

# .orange[Gitlab pour les SHS]<br/> .small[Documenter et gérer ses projets numériques<br/> avec Gitlab]



.sub[
### Nicolas Sauret<br/> .small[[FP Lab](https://fplab.parisnanterre.fr) (Labex PP, UPN)]
]

.footer[
Ateliers FP Lab &ndash; 24 mars 2022
]


---
class: left, middle, flexy

.lefty[
## Déroulé de l'atelier
]
.rigthy.encart.large[

1. Introduction  
1. Quelques usages en SHS  
1. Prise en main

]
???

---
class: center, middle

# Qu'est-ce que .orange[GitLab] ?

---
class: center, middle

# Git + Lab

---
class: left, middle,

.flexy[
.lefty.left[
## .orange[Git] : une forge logicielle

- Un protocole d’échange et de synchronisation de fichiers
- Un système de suivi de versions
- Un registre (principe de la forge)

**→ produire du code, du texte, des données selon un protocole rigoureux**
]
.righty[
.shadow[![Git](https://i.stack.imgur.com/MZsif.png)]
]]
???

---
class: left, middle,

.flexy[
.lefty.left[
## .orange[Gitlab] : une plateforme collaborative

- Fonctionnalités communautaires
- Gestion de groupes, de membres
- Documentation (wiki, readme)
- Gestion de projets (tickets, jalons, tableau de bord)

**→ travailler en collectif**

**→ ouvrir et partager la recherche en train de se faire (ou pas)**
]
.righty[
.shadow[![gitlab](https://images.itnewsinfo.com/lmi/articles/grande/000000076724.png)]
]]
???

---
class: left, middle, flexy

.lefty[
# .orange[Quelques notions]

## exemples et exercices
]
.righty.encart[
1. répertoire
1. commit
2. branche
3. forge
4. historique
4. issue
5. readme
6. wiki
7. ...
]

???

1. commit
2. branche
3. forge
2. historique, blame,
3. forge: fork, branch, pull request, merge
4. issue (+ board)
5. readme
6. wiki
7.
]
---
class: left, middle

## Répertoire

- "_repository_", ou encore "projet"
- un dossier contenant des sous-dossiers et des fichiers
- un registre dédié
- des fonctionnalités collaboratives

---
class: left, middle

.encart[
### .hsmall[![](media/gitlab-fox.png)] &ndash; exercice : Premiers pas

1. Connexion à Gitlab
1. Rejoindre le groupe Gitlab de l'atelier: `fplab-ateliergitlab`
2. Ouvrir le répertoire: `fplab-ateliergitlab/supportatelier`
2. Premiers pas : [`premierspas.md`](https://gitlab.huma-num.fr/fplab-ateliergitlab/supportatelier/-/tree/main)
   1. Créer un répertoire
]
---
class: left, middle

## Commit

- Déclaration de modifications de un ou plusieurs fichiers
- Documentation des modifications
- Enregistrement → inscription au registre

→ défini par: identifiant, date, auteur, fichiers concernés, différences avec l'état précédent

---
class: left, middle

.encart[
### .hsmall[![](media/gitlab-fox.png)] &ndash; exercice : Premiers pas

1. Poursuivre avec [`premierspas.md`](https://gitlab.huma-num.fr/fplab-ateliergitlab/supportatelier/-/tree/main) → `Éditer un fichier`
]


---
class: left, middle, flexy

.lefty[
## Branche

- divergence
- test/erreur
- collaboration
- exploration

→ au sein d'un même répertoire

]
.righty.shadow[
[![](media/gitlab-branch.png)](https://framagit.org/ecrinum/ecridil-booksprint/-/network/master)
]


---
class: left, middle
.shadow[
[![](media/gitlab-branch.png)](https://framagit.org/ecrinum/ecridil-booksprint/-/network/master)
]
---
class: left, middle, flexy

.lefty[
## _Fork_

- divergence
- test/erreur
- collaboration
- exploration

→ dans un autre répertoire/registre
]
.righty.shadow[
.p40image[![](media/gitlab-fork.png)]
]
---
class: left, middle

.encart[
### .hsmall[![](media/gitlab-fox.png)] &ndash; exercice : Fork

1. Dans le répertoire: `fplab-ateliergitlab/supportatelier`
2. Ouvrir [`cv.md`](https://gitlab.huma-num.fr/fplab-ateliergitlab/supportatelier/-/blob/main/cv.md)
3. Suivre les instructions `"Forker un projet"`
]


---
class: left, middle, flexy

.lefty[
## Forge

- fusion des modifications
- résolution de tous les commits d'une branche donnée
]
.righty.encart[
- `Fork`
- `Merge`
- `Pull Request`
- `Conflict`
]
---
class: left, middle, flexy

.lefty[
## Historique

→ différentes représentations des actions: listes, comparateur ligne à ligne, graphes

- historique des commits : au niveau du répertoire  
  [→ le blog du HN Lab](https://gitlab.huma-num.fr/hnlab/blog/-/tree/master/_posts)
- historique des modifications : au niveau d'un fichier  
  [→ la documentation d'Huma-Num](https://gitlab.huma-num.fr/huma-num-public/documentation/-/blame/master/mkdocs.yml)
]
.righty[
.encart[
- `Commits`
- `Comparateur ligne à ligne`
- `Blame`
- `History`
]


]

---
class: left, middle

.encart[
### .hsmall[![](media/gitlab-fox.png)] &ndash; exercice : Fork

1. Reprendre [`cv.md`](https://gitlab.huma-num.fr/fplab-ateliergitlab/supportatelier/-/blob/main/cv.md)
3. Suivre les instructions `"Modifier le CV"`
]



---
class: left, middle

## (Intégration continue ?)


---
class: left, middle, flexy

.lefty[
## Issue (ou ticket)

- retour d'usages (bugs)
- collaboration  
  [→ issue Stylo [Github]](https://github.com/EcrituresNumeriques/stylo/issues/597)
- liste de tâches, liste de souhaits, feuille de route
- structuration de l'activité (jalons, catégories, échéance, attribution)
- tableau de bord  
  [→ board HNSO](https://gitlab.huma-num.fr/huma-num/hnso-cctp/-/boards/593)
]
.righty.encart[
- `Issues`
- `Boards`
- `Milestones`
]
---
class: left, middle

.encart[
### .hsmall[![](media/gitlab-fox.png)] &ndash; exercice : Issue

1. Dans le répertoire: `fplab-ateliergitlab/supportatelier`
2. Ouvrir [`issue.md`](https://gitlab.huma-num.fr/fplab-ateliergitlab/supportatelier/-/blob/main/issue.md)
3. Suivre les instructions `"Créer une issue"`
]



---
class: left, middle, flexy

.lefty[
## Documentation

- readme.md
- licence
- wiki

→ format Markdown
]
.righty.shadow[
![](media/gitlab-readme.png)
]

---
class: left, middle

## .orange[Quelques scénarios de gestion de données]

- La gestion des sources d'ISIDORE ([correction mineure](https://gitlab.huma-num.fr/huma-num/isidore-sources-xml/-/commit/43916cf1eb43baa786464cb2b775f2ceb9ebc17c))
- Le cas de la revue _Sens Public_ ([sens-public.org](https://sens-public.org))
  - [édition](https://framagit.org/laconis/SP-articles/-/commit/277301207396abc9748772d69a6e7cddffceea48)
  - [coquille](https://framagit.org/laconis/SP-articles/-/commit/1c1aa6ee226c7ac2666f21ebf6b8d003881133b4)
- Le cas de La Grande Encyclopédie : édition en XML TEI
  - [suivi éditorial](https://gitlab.huma-num.fr/disco-lge/tei-encoding/-/commit/4cf197c01b99614dbce5ea2947dc2b4530a66e37)
- Le cas de la ZAP Rimbaud : co-édition
  - [hacker _Une saison en enfer_](https://gitlab.com/antilivre/rimbaud.zap/-/network/master)


---
class: left, middle

## Git en local !

Synchroniser un projet avec une instance sur sa machine personnelle.

.encart[
### .hsmall[![](media/gitlab-fox.png)] &ndash; exercice : Git en local

1. Dans le support de l'atelier, ouvrir [`git-en-local.md`](https://gitlab.huma-num.fr/fplab-ateliergitlab/supportatelier/-/blob/main/git-en-local.md)
2. Suivre les instructions
]
